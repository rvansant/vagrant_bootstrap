# A.J.'s Vagrant Bootstrap
A dead-simple LAMP stack without any bells and whistles for your basic Linux/Apache/MySQL/PHP install, using Chef Solo for provisioning.

## Requirements
* [VirtualBox](https://www.virtualbox.org)
* [Vagrant v1.0.x](http://vagrantup.com)
* [vagrant-hostmaster](https://github.com/mosaicxm/vagrant-hostmaster)
	* `vagrant gem install vagrant-hostmaster`

## Setup
* Create repo for your project
* cd to project directory
* Run `git submodule add git@bitbucket.org:northps/vagrant_bootstrap.git`
* Run `git submodule update --init --recursive`
* `cp vagrant_bootstrap/sample.Vagrantfile Vagrantfile`
* Update Vagrantfile for your project
* Run `vagrant up`

## Default Host
[dev.local](http://dev.local)

## MySQL
* Username: root
* Password: root
* Host: localhost
* Port: 3306
* If dump.sql exists in project root, it will be imported during provisioning

## Installed software
* Apache 2
* MySQL
* PHP 5.4 (with mysql, curl, mcrypt, memcached, gd)
* memcached
* postfix
* vim, git, screen, curl, composer